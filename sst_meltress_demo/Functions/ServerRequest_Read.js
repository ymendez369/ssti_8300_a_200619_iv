﻿// Keep this lines for a best effort IntelliSense of Visual Studio 2017.
/// <reference path="C:\TwinCAT\Functions\TE2000-HMI-Engineering\Infrastructure\TcHmiFramework\Latest\Lib\jquery.d.ts" />
/// <reference path="C:\TwinCAT\Functions\TE2000-HMI-Engineering\Infrastructure\TcHmiFramework\Latest\TcHmi.d.ts" />
/// <reference path="C:\TwinCAT\Functions\TE2000-HMI-Engineering\Infrastructure\TcHmiFramework\Latest\Controls\System\TcHmiControl\Source.d.ts" />

// Keep this lines for a best effort IntelliSense of Visual Studio 2013/2015.
/// <reference path="C:\TwinCAT\Functions\TE2000-HMI-Engineering\Infrastructure\TcHmiFramework\Latest\Lib\jquery\jquery.js" />
/// <reference path="C:\TwinCAT\Functions\TE2000-HMI-Engineering\Infrastructure\TcHmiFramework\Latest\TcHmi.js" />

(function (TcHmi) {

    var ServerRequest_Read = function (ctx,SymbolString) {




        if (TcHmi.Server.isWebsocketReady()) {
            // Request object.
            var request = {
                'requestType': 'ReadWrite',
                'commands': [
                    {
                        'symbol': SymbolString
                    }
                ]
            };

            // Send request to TwinCAT HMI Server.
            TcHmi.Server.request(request, function (data) {
                // Callback handling.
                if (data.error !== TcHmi.Errors.NONE) {
                    // Handle TcHmi.Server class level error here.
                    ctx.error(JSON.stringify(data.error));
                }
                var response = data.response;
                if (response.error !== undefined) {
                    // Handle TwinCAT HMI Server response level error here.
                    ctx.error(JSON.stringify(response));
                }
                var commands = response.commands;
                if (commands === undefined) {
                    ctx.error(JSON.stringify(commands));
                }
                for (var i = 0, ii = commands.length; i < ii; i++) {
                    var command = response.commands[i];
                    if (command === undefined) {
                        ctx.error(JSON.stringify(command));
                    }
                    if (command.error !== undefined) {
                        // Handle TwinCAT HMI Server command level error here.
                        ctx.error(JSON.stringify(command.error));
                    }
                    // Handle result...
                    console.log(command.readValue);
                    ctx.success(JSON.stringify(command.readValue,null,4));
                }
            });
        }




    };
    
    TcHmi.Functions.registerFunction('ServerRequest_Read', ServerRequest_Read);
})(TcHmi);